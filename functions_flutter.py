#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 15 11:00:00 2024

@author: ech022
"""

import numpy as np

def func(t, Z, DATA, Term, NUM):
    """
    This function evaluates the governing system of ODEs.

    Inputs:
    - t: real variable (current time)
    - Z: current state of the system. Array of dimension 8x1
    - DATA: dictionary containing all the parameters of the problem under study
    - Term: dictionary containing auxiliary matrices
    - NUM: dictionary containing parameters associated with numerical integration

    Outputs:
    - DZ: Derivative of Z with respect to time. Array of dimension 8x1
    """
    # Extract variables from DATA
    ra = DATA['ra']
    xa = DATA['xa']
    ah = DATA['ah']
    mu = DATA['mu']
    Beta = DATA['Beta']
    Gamma = DATA['Gamma']
    Omb = DATA['Omb']
    Uinf = DATA['Uinf']
    Psi1 = DATA['Psi1']
    Psi2 = DATA['Psi2']
    Eps1 = DATA['Eps1']
    Eps2 = DATA['Eps2']

    DPhi = Psi1*Eps1*np.exp(-Eps1*t) + Psi2*Eps2*np.exp(-Eps2*t)

    B0 = 0.5 - ah
    B1 = 0.5 + ah

    Aa = np.zeros((2, 2))
    Aa[0, 0] = -2/mu*DPhi
    Aa[0, 1] = -2/mu*B0*DPhi
    Aa[1, 0] = 2*B1/(mu*ra**2)*DPhi
    Aa[1, 1] = 2*B1/(mu*ra**2)*B0*DPhi

    Q0 = np.array([[NUM['CI'][0]], [NUM['CI'][1]]])

    Ms = np.array([[1, xa*np.cos(Z[1])], [xa/ra**2*np.cos(Z[1]), 1]])

    Gs = np.zeros((2, 1))
    Gs[0] = -xa*np.sin(Z[1])*Z[3]**2

    Fs = np.zeros((2, 1))
    Fs[0] = Omb**2/Uinf**2 * (Z[0] + Gamma*Z[0]**3)
    Fs[1] = 1/Uinf**2 * (Z[1] + Beta*Z[1]**3)

    # Solving the linear system for DZ
    Dq = Z[2:4]
    q = Z[0:2]
    W = Z[4:8]

    DZ = np.zeros((8, 1))

    DZ[0] = Z[2]
    DZ[1] = Z[3]

    # Calculate the right-hand side of the equation

    SaW = Term['Sa'] @ W
    
    rhs = (-Term['Cs'] @ Dq - Fs.flatten() - Gs.flatten() -
           Term['Ca'] @ Dq - Term['Ka'] @ q - (Aa @ Q0).flatten()  - SaW)
    
    Mtot = Ms + Term['Ma']
    # Solve the linear system for DZ[2:4]
    DZ[2:4] = np.linalg.solve(Mtot, rhs.reshape(-1, 1))

    DZ[4] = Z[0] - DATA['Eps1']*Z[4]
    DZ[5] = Z[0] - DATA['Eps2']*Z[5]
    DZ[6] = Z[1] - DATA['Eps1']*Z[6]
    DZ[7] = Z[1] - DATA['Eps2']*Z[7]

    return DZ.flatten()



def matrices(DATA):
    """
    This function evaluates computes the aerodynamic and structural matrices
    which are independent of the configuration and/or time.

    Inputs:
    - DATA: dictionary containing all the parameters of the problem under study.
    
    Outputs:
    - Term: dictionary containing aerodynamic and structural matrices.
    """
    # Extract variables from DATA
    Xi_h = DATA['Xi_h']
    Xi_a = DATA['Xi_a']
    mu = DATA['mu']
    ra = DATA['ra']
    ah = DATA['ah']
    xa = DATA['xa']
    Omb = DATA['Omb']
    Beta = DATA['Beta']
    Gamma = DATA['Gamma']
    Uinf = DATA['Uinf']
    Psi1 = DATA['Psi1']
    Psi2 = DATA['Psi2']
    Eps1 = DATA['Eps1']
    Eps2 = DATA['Eps2']

    Phi0 = 1 - Psi1 - Psi2
    DPhi0 = Psi1*Eps1 + Psi2*Eps2

    B0 = 0.5 - ah
    B1 = 0.5 + ah

    # Structural matrices
    Cs = np.zeros((2, 2))
    Cs[0, 0] = 2*Xi_h*Omb/Uinf
    Cs[1, 1] = 2*Xi_a/Uinf

    # Aerodynamic matrices
    Ma = np.zeros((2, 2))
    Ma[0, 0] = 1/mu
    Ma[0, 1] = -ah/mu
    Ma[1, 0] = -ah/(mu*ra**2)
    Ma[1, 1] = 1/(mu*ra**2) * (ah**2 + 1/8)

    Ca = np.zeros((2, 2))
    Ca[0, 0] = 2/mu*Phi0
    Ca[0, 1] = 1/mu * (1 + 2*B0*Phi0)
    Ca[1, 0] = -2*B1/(mu*ra**2) * Phi0
    Ca[1, 1] = B0/(mu*ra**2) * (1 - 2*B1*Phi0)

    Ka = np.zeros((2, 2))
    Ka[0, 0] = 2/mu*DPhi0
    Ka[0, 1] = 2/mu * (Phi0 + B0*DPhi0)
    Ka[1, 0] = -2*B1/(mu*ra**2) * DPhi0
    Ka[1, 1] = -2*B1/(mu*ra**2) * (Phi0 + B0*DPhi0)

    Sa = np.zeros((2, 4))
    Sa[0, 0] = -2/mu * Eps1**2 * Psi1
    Sa[0, 1] = -2/mu * Eps2**2 * Psi2
    Sa[0, 2] = 2/mu * (1 - B0*Eps1) * Eps1 * Psi1
    Sa[0, 3] = 2/mu * (1 - B0*Eps2) * Eps2 * Psi2
    Sa[1, 0] = 2*B1/(mu*ra**2) * Eps1**2 * Psi1
    Sa[1, 1] = 2*B1/(mu*ra**2) * Eps2**2 * Psi2
    Sa[1, 2] = -2*B1/(mu*ra**2) * (1 - B0*Eps1) * Eps1 * Psi1
    Sa[1, 3] = -2*B1/(mu*ra**2) * (1 - B0*Eps2) * Eps2 * Psi2

    # Return the matrices in a dictionary
    Term = {'Cs': Cs, 'Ma': Ma, 'Ca': Ca, 'Ka': Ka, 'Sa': Sa}
    return Term
